*JDBC (Java Database Connectivity) - это API (Application Programming Interface) для языка Java,
предоставляющее доступ к различным базам данных с использованием стандартных SQL-запросов.
Подключение к базе данных PostgreSQL с использованием JDBC включает несколько шагов:*

1. Загрузка драйвера JDBC для PostgreSQL.
2. Установление соединения с базой данных.
3. Выполнение SQL-запросов.
4. Обработка результатов.

## Некоторые полезные команды

**Проверить запущена ли PostgreSQL:**
`sudo service postgresql status`

*Войти в в панель управления PostgreSQL под юзером postgres:*
`sudo -u postgres psql`

*Войти в определённую базу данных под определённым юзером:*
`psql -U exp_user -d db_exp -W`
*Ввести пароль*
*Если ошибка, то из под sudo исправить конфиг pg_hba.conf, заменить все peer на md5, а после рестартнуть service postgresql*

*Для просмотра списка ВСЕХ профилей пользователей:*
`\du`
*либо*
`SELECT rolname FROM pg_roles;`

*Для просмотра пользовательских профилей, которым разрешён вход в систему:*
`SELECT rolname FROM pg_roles WHERE rolcanlogin = true;`

*Создание нового пользователя:*
`CREATE ROLE exp_user WITH LOGIN PASSWORD 'o1aa1o';`
`CREATE USER new_username WITH PASSWORD 'password';`

*Удаление пользователя:*
`DROP USER new_username;`

*Создание новой базы данных:*
`CREATE DATABASE your_database;`

*Назначение базы данных для пользователя:*
`GRANT ALL PRIVILEGES ON DATABASE your_database TO new_username;`

*Просмотреть все созданные БД:*
`\l`

*либо с помощью SQL-запроса к таблице pg_database*
`SELECT datname FROM pg_database;`

*Удалить БД:*
`DROP DATABASE mydatabase;`

*Для просмотра всех созданных таблиц:*
`\dt`

*Вывести данные в таблице:*
`SELECT * FROM table_name`

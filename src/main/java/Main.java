import java.sql.*;

public class Main {
    public static void main(String[] args) {
        Connection connection = null;
        try {
            /* Загрузка драйвера JDBC для PostgreSQL.
               Регистрация драйвера и доступ к функциональности JDBC */
            Class.forName("org.postgresql.Driver");

            /*Уставновление соединения с базой данных.
              Connection - интерфейс с методами для взаимодействия с БД */
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/db_exp",
                    "exp_user", "o1aa1o");

            /*Создание таблицы. Statement - интерфейс для выполнения SQL-запросов к БД.
              executeUpdate используется для выполнения SQL-запросов, которые изменяют данные
              в базе данных (например, INSERT, UPDATE, DELETE).
              Он возвращает количество строк, затронутых запросом.*/
            Statement statement = connection.createStatement();
            String createTableSQL = "CREATE TABLE IF NOT EXISTS employees " +
                    "(id SERIAL PRIMARY KEY, " +
                    "name VARCHAR(255), " +
                    "age INT)";
            statement.executeUpdate(createTableSQL);
            System.out.println("Успешное создание таблицы");

            //Вставка данных
            String insertSQL = "INSERT INTO employees (name, age) VALUES ('Егор Мартынов', 24)";
            statement.executeUpdate(insertSQL);
            System.out.println("Успешная вставка данных");

            /* Выборка и вывод данных.
                resultSet представляет собой объект, содержащий результаты SQL-запроса.
                Методы get... используются для извлечения значений из результирующего набора данных.
                executeQuery используется для выполнения SQL-запросов, которые возвращают данные из базы данных (например, SELECT).
                Он возвращает объект ResultSet, который содержит результаты запроса.*/

            String selectSQL = "SELECT * FROM employees";
            ResultSet resultSet = statement.executeQuery(selectSQL);
            while (resultSet.next()) {
                System.out.println("ID: " + resultSet.getInt("id") +
                        ", Имя: " + resultSet.getString("name") +
                        ", Возраст: " + resultSet.getInt("age"));
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {
                    connection.close(); //закрывает соединение с базой данных после выполнения всех операций
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
